import { readFile } from "fs";

readFile(process.argv[2], function (err, fileData) {
    if (err === null) {
        let indexA = 0,
            indexB = 0,
            indexC = 0,
            word = "",
            wordLength = 0,
            wordsLength = 0;
        const fileString = fileData.toString(),
            output = [],
            lines = fileString.replace(/\r\n/g, "\n").split("\n"),
            // get the grid dimensions from the input according to the directions so that the grid is differentiated from the following word list
            size = (function () {
                const sizes = lines[0].split("x");
                return [Number(sizes[0]), Number(sizes[1])];
            }()),
            // build the grid, a table of letters
            grid = (function () {
                let count = 1;
                const max = size[1] + 1,
                    table = [];
                if (max > 1) {
                    do {
                        table.push(lines[count].toLowerCase().split(" "));
                        count = count + 1;
                    } while (count < max);
                }
                return table;
            }()),
            // build the word list
            words = (function () {
                let count = size[1] + 1,
                    item = "";
                const max = lines.length,
                    list = [];
                if (max > count) {
                    do {
                        if (lines[count].replace(/\s+/, "") !== "") {
                            item = lines[count].replace(/\s/g, "").toLowerCase();
                            list.push(item);
                        }
                        count = count + 1;
                    } while (count < max);
                }
                return list;
            }()),
            // this function determines if a word is found provided a direction and coordinates according to a two character starting sequence
            // once the complete word is found in the grid it is removed from the word list
            scan = function (direction, reverse) {
                let letter = 0;
                const word = words[indexC],
                    wordLen = word.length,
                    coord = [indexA, indexB],
                    startX = indexB,
                    startY = indexA,
                    // account for word in reverse order otherwise enumerating the same logic
                    dirWord = (reverse === true)
                        ? word.split("").reverse().join("")
                        : word;
                do {
                    // exit when the direction from the provided start point does not match the word
                    if (dirWord[letter] !== grid[coord[0]][coord[1]]) {
                        return false;
                    }
                    letter = letter + 1;

                    // word fully scanned
                    if (letter === wordLen) {
                        if (reverse === true) {
                            output.push(`${word} ${coord[0]}:${coord[1]} ${startY}:${startX}`);
                        } else {
                            output.push(`${word} ${startY}:${startX} ${coord[0]}:${coord[1]}`);
                        }
                        words.splice(indexC, 1);
                        wordsLength = wordsLength - 1;
                        indexC = indexC - 1;
                        indexA = size[1];
                        indexB = size[0];
                        return true;
                    }

                    // increment coordinates according to direction
                    if (direction === "vertical" || direction === "diagonal-left" || direction === "diagonal-right") {
                        coord[0] = coord[0] + 1;
                    }
                    if (direction === "horizontal" || direction === "diagonal-right") {
                        coord[1] = coord[1] + 1;
                    } else if (direction === "diagonal-left") {
                        coord[1] = coord[1] - 1;
                    }
                } while (
                    // break the loop based upon word length and grid size
                    coord[0] < size[1] &&
                    (
                        (direction === "diagonal-left" && coord[1] > -1) ||
                        (direction !== "diagonal-left" && coord[1] < size[0])
                    ) &&
                    letter < wordLen
                );
                return false;
            };
        wordsLength = words.length;
        if (grid.length > 0) {
            // word list
            do {
                word = words[indexC];
                wordLength = word.length;
                indexA = 0;
                // grid vertical
                do {
                    indexB = 0;
                    // grid horizontal
                    do {
                        // identify a start point by matching the first two characters of a word in sequential or reverse order to any 4 directions on the grid
                        // 4 directions:
                        // * vertical
                        // * horizontal
                        // * vertical-left, from upper left to the lower right
                        // * vertical-right, from upper right to the lower left
                        if (grid[indexA][indexB] === word.charAt(0)) {
                            // forward match
                            if (
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB] === word.charAt(1) &&
                                scan("vertical", false) === true
                            ) {
                                break;
                            }
                            if (
                                size[0] - indexB >= wordLength &&
                                grid[indexA][indexB + 1] === word.charAt(1) &&
                                scan("horizontal", false) === true
                            ) {
                                break;
                            }
                            if (
                                size[0] - indexB >= wordLength &&
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB + 1] === word.charAt(1) &&
                                scan("diagonal-right", false) === true
                            ) {
                                break;
                            }
                            if (
                                indexB > 0 &&
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB - 1] === word.charAt(1) &&
                                scan("diagonal-left", false) === true
                            ) {
                                break;
                            }
                        }
                        if (grid[indexA][indexB] === word.charAt(wordLength - 1)) {
                            // reverse match
                            if (
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB] === word.charAt(wordLength - 2) &&
                                scan("vertical", true) === true
                            ) {
                                break;
                            }
                            if (
                                size[0] - indexB >= wordLength &&
                                grid[indexA][indexB + 1] === word.charAt(wordLength - 2) &&
                                scan("horizontal", true) === true
                            ) {
                                break;
                            }
                            if (
                                size[0] - indexB >= wordLength &&
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB + 1] === word.charAt(wordLength - 2) &&
                                scan("diagonal-right", true) === true
                            ) {
                                break;
                            }
                            if (
                                indexB > 0 &&
                                size[1] - indexA >= wordLength &&
                                grid[indexA + 1][indexB - 1] === word.charAt(wordLength - 2) &&
                                scan("diagonal-left", true) === true
                            ) {
                                break;
                            }
                        }
                        indexB = indexB + 1;
                    } while (indexB < size[0]);
                    indexA = indexA + 1;
                } while (indexA < size[1]);
                indexC = indexC + 1;
            } while (indexC < wordsLength);
        }
        process.stdout.write(output.join("\n"));
    } else {
        console.log("Error reading file!");
        process.stderr.write("Error reading file!");
        process.stderr.write(err);
        process.exit(1);
    }
});